/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/03/2020
 */
#include <stdio.h>
#include <string.h>

// Alcune dei seguenti valori sono stati assegnati arbitrariamente dopo averne discusso in una VC.
#define MAX_ROW_LENGTH 200
#define MAX_WORD_LENGTH 50
#define MAX_CRYPT_WORD_LENGTH 100
#define MAX_DICT_WORDS 30

// Definizione della struct in cui andò a memorizzare le parole e le ricodifiche
typedef struct {
    char crypt [MAX_CRYPT_WORD_LENGTH + 1];
    char original [MAX_WORD_LENGTH + 1];
} dictWord;

int main() {
    int dictWordsNumber;
    dictWord dictionary[MAX_DICT_WORDS];
    char row[MAX_ROW_LENGTH + 1];
    int count = 0;
    int found;

    //Apro i file dizionario, input e output
    FILE* dictFile = fopen("dizionario.txt", "r");
    FILE* sourceFile = fopen("sorgente.txt", "r");
    FILE* outputFile = fopen("ricodifica.txt", "w");

    // Gestione degli eventuali errori legati ai file.
    if (dictFile == NULL || sourceFile == NULL) {
        printf("Invalid file.");
        return -1;
    }

    // Leggo il numero di coppie del dizionario.
    fscanf(dictFile, "%d", &dictWordsNumber);

    // Inserisco nel vettore dizionario le coppie presenti nel file.
    while (!feof(dictFile) && count <= dictWordsNumber) {
        fscanf(dictFile, "%s %s", dictionary[count].crypt, dictionary[count].original);
        count ++;
    }

    // Chiudo il dile dizionario.
    fclose(dictFile);

    while(!feof(sourceFile)) {
        // Leggo una riga, conosciamo la lunghezza massima.
        fgets(row, MAX_ROW_LENGTH + 1, sourceFile);

        // Per ogni carattere della riga,
        for (int i = 0; i < strlen(row); i++) {
            // Inizialmente la flag è impostata a false
            found = 0;
            // Per ogni coppia nel dizionario,
            for (int j = 0; (j < dictWordsNumber && !found); j++) {
                // Imposto la flag a true
                found = 1;
                // Per ogni lettera della parola nel dizionario,
                for (int k = 0; k < strlen(dictionary[j].original); k++) {
                    // Se le lettere non corrispondono, significa che la parola presente all'indice j non è un rimpiazzo valido.
                    if (row[i + k] != dictionary[j].original[k]) {
                        found = 0;
                    }
                }
                if (found) {
                    // Stampo sul file di output la stringa di rimpiazzo.
                    fprintf(outputFile, "%s", dictionary[j].crypt);
                    // Incremento i della lunghezza della parola che ho sostituito - 1 (poichè il for aumenterà ancora
                    // di 1 la variabile i alla prossima iterazione).
                    i = i + (strlen(dictionary[j].original) - 1);
                }
            }
            if (!found) {
                // Se non ho trovato nessun rimpiazzo, stampo il carattere nel file; molto utile poichè stampa anche
                // eventuali <code>\n</code>, così l'output avrà le righe formattate esattamente come l'input.
                fprintf(outputFile, "%c", row[i]);
            }
        }
    }

    //Chiusura dei file
    fclose(sourceFile);
    fclose(outputFile);

    return 0;
}
