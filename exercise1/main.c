/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/03/2020
 */

#include <stdio.h>
#define MAX_DIM 20

int main(int argc, char* argv[]) {
    int matrix[MAX_DIM][MAX_DIM] = {-1};
    int rows = 0;
    int columns = 0;
    int max;
    int maxRow;
    int sum;

    // argv[0] è il nome del programma, mentre argv[1] è il nome del file di input.
    if (argc != 2) {
        printf("Invalid arguments number!");
        return -1;
    }

    FILE* fo = fopen(argv[1], "r");

    // Errori relativi al file.
    if (fo == NULL) {
        printf("Invalid file.");
        return -1;
    }

    // Leggo il numero di righe e colonne
    fscanf(fo, "%d %d", &rows, &columns);

    // Leggo la matrice.
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            fscanf(fo, "%d", &matrix[i][j]);
        }
    }

    fclose(fo);

    // Per ogni colonna (giornata),
    for (int c = 0; c < columns; c++) {
        max = 0;
        maxRow = -1;
        // Per ogni riga (squadra),
        for (int r = 0; r < rows; r++) {
            sum = 0;
            // Sommo le prime `c` colonne (giornate).
            for (int j = 0; j <= c; j++) {
                sum += matrix[r][j];
            }
            // Se il punteggio è maggiore del massimo, abbiamo una nuova capolista.
            if (sum > max) {
                max = sum;
                maxRow = r;
            }
        }
        // Stampo il risultato,
        // `c + 1` perchè non avrebbe senso `giorno 0`
        printf("The best team for the day number %d is: %d\n", c + 1, maxRow);
    }

    return 0;
}
