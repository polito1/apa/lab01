/*
 * @author Simone Nicol <en0mia.dev@gmail.com>
 * @created 10/09/2020
 */

#include <stdio.h>
#include <string.h>
#define MAX_COMMAND_LINE_ROW_LENGTH 100
#define MAX_MATRIX_ROWS 30
#define MAX_MATRIX_COLUMNS 30
#define MAX_FILENAME_LENGTH 20
#define MAX_SELECTOR_LENGTH 7
#define MAX_DIRECTION_LENGTH 8

void downColumn(int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int index, int positions, int rows, int columns);
void upColumn(int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int index, int positions, int rows, int columns);
void rightRow(int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int index, int positions, int rows, int columns);
void leftRow(int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int index, int positions, int rows, int columns);
void printMatrix(int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int rows, int columns);

int main() {
    int matrix [MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS] = {0};
    int rows;
    int columns;
    char fileName[MAX_FILENAME_LENGTH + 1];
    char sel[MAX_SELECTOR_LENGTH + 1];
    int index;
    char direction[MAX_DIRECTION_LENGTH + 1];
    int positions;
    char command[MAX_COMMAND_LINE_ROW_LENGTH + 1];
    int converted;

    FILE* fi;

    printf("Enter the input file name: (max 20 chars) ");
    scanf("%s", fileName);

    fi = fopen(fileName, "r");

    // Gestione degli errori legati al file.
    if (fi == NULL) {
        printf("Invalid file.");
        return -1;
    }

    // Leggo il numero di righe e colonne.
    if (fscanf(fi, "%d %d", &rows, &columns) != 2) {
        printf("Invalid file format.");
        return -1;
    }

    // Creo la matrice leggendo i valori da file.
    for (int i = 0; (i < rows && !feof(fi)); i++) {
        for (int j = 0; j < columns; j++) {
            fscanf(fi, "%d", &matrix[i][j]);
        }
    }

    fclose(fi);

    // Puliamo il buffer da eventuali caratteri rimasti dalla scanf().
    gets(command);

    // Ciclo terminato dall'interno dai return.
    while (1) {
        printf("Enter data in the format `<selector> <index> <direction> <positions>` (`fine` to stop): ");
        // Leggo le istruzioni.
        fgets(command, MAX_COMMAND_LINE_ROW_LENGTH + 1, stdin);
        // Divido la stringa nei vari componenti del comando.
        converted = sscanf(command, "%s %d %s %d", sel, &index, direction, &positions);

        // Se l'utente ha inserito `fine` devo uscire.
        if (strcmp(sel, "fine") == 0) {
            return 0;
        }

        // Errore di formattazione dei parametri, uscita.
        if (converted != 4) {
            printf("Invalid input.");
            return -1;
        }

        if (strcmp(sel, "riga") == 0) {
            if (strcmp(direction, "destra") == 0) {
                // Richiamo il metodo per spostare una riga a destra, notiamo `index - 1` poichè l'utente conta
                // a partire da 1.
                rightRow(matrix, index - 1, positions, rows, columns);
                // Stampo la matrice modificata.
                printMatrix(matrix, rows, columns);
            } else if (strcmp(direction, "sinistra") == 0) {
                // Richiamo il metodo per spostare una riga a sinistra, notiamo `index - 1` poichè l'utente conta
                // a partire da 1.
                leftRow(matrix, index - 1, positions, rows, columns);
                // Stampo la matrice modificata.
                printMatrix(matrix, rows, columns);
            }
        } else if (strcmp(sel, "colonna") == 0) {
            if (strcmp(direction, "su") == 0) {
                // Richiamo il metodo per spostare una colonna in su, notiamo `index - 1` poichè l'utente conta
                // a partire da 1.
                upColumn(matrix, index - 1, positions, rows, columns);
                // Stampo la matrice modificata.
                printMatrix(matrix, rows, columns);
            } else if (strcmp(direction, "giu") == 0) {
                // Richiamo il metodo per spostare una colonna in giù, notiamo `index - 1` poichè l'utente conta
                // a partire da 1.
                downColumn(matrix, index - 1, positions, rows, columns);
                // Stampo la matrice modificata.
                printMatrix(matrix, rows, columns);
            }
        } else {
            // Istruzioni invalide, lo comunico all'utente e passo alla prossima iterazione.
            printf("Invalid instructions.\n");
        }
    }
}

// Metodo ricorsivo per spostare la colonna in giù del numero di posizioni definito nella variabile positions.
void downColumn(int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int index, int positions, int rows, int columns)
{
    // Termine della ricorsione.
    if (positions == 0) {
        return;
    }

    // Right shift del vettore colonna in posizione index.
    int tmp = matrix[rows - 1][index];
    for (int i = rows - 1; i > 0; i--) {
        matrix[i][index] = matrix[i - 1][index];
    }
    matrix[0][index] = tmp;

    // Richiamo ricorsivo.
    downColumn(matrix, index, positions - 1, rows, columns);
}

// Metodo ricorsivo per spostare la colonna in su del numero di posizioni definito nella variabile positions.
void upColumn(int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int index, int positions, int rows, int columns)
{
    // Termine della ricorsione.
    if (positions == 0) {
        return;
    }

    // Left shift del vettore colonna in posizione index.
    int tmp = matrix[0][index];
    for (int i = 0; i < rows; i++) {
        matrix[i][index] = matrix[i + 1][index];
    }
    matrix[rows - 1][index] = tmp;

    // Richiamo ricorsivo.
    upColumn(matrix, index, positions - 1, rows, columns);
}

// Metodo ricorsivo per spostare la riga a destra del numero di posizioni definito nella variabile positions.
void rightRow(int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int index, int positions, int rows, int columns)
{
    // Termine della ricorsione.
    if (positions == 0) {
        return;
    }

    // Right shift del vettore riga in posizione index.
    int tmp = matrix[index][columns - 1];
    for (int i = columns - 1; i > 0; i--) {
        matrix[index][i] = matrix[index][i - 1];
    }
    matrix[index][0] = tmp;

    // Richiamo ricorsivo
    rightRow(matrix, index, positions - 1, rows, columns);
}

// Metodo ricorsivo per spostare la riga a sinistra del numero di posizioni definito nella variabile positions.
void leftRow(int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int index, int positions, int rows, int columns)
{
    // Termine della ricorsione.
    if (positions == 0) {
        return;
    }

    // Left shift del vettore riga in posizione index.
    int tmp = matrix[index][0];
    for (int i = 0; i < columns; i++) {
        matrix[index][i] = matrix[index][i + 1];
    }
    matrix[index][columns - 1] = tmp;

    // Richiamo ricorsivo.
    leftRow(matrix, index, positions - 1, rows, columns);
}

// Funzione per stampare la matrice a video, cella per cella.
void printMatrix(int matrix[MAX_MATRIX_ROWS][MAX_MATRIX_COLUMNS], int rows, int columns)
{
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}
